﻿using UnityEngine;
using UnityEngine.UI;

public class UICanvasScaler : MonoBehaviour {

	private const float PIXEL_PER_UNIT = 100f;

	private const float TARGET_HEIGHT = 1710f;

	private const float TARGET_WIDTH = 2732f;

    void Awake()
	{
		CanvasScaler canvasScaler = GetComponent<CanvasScaler>();

		canvasScaler.referenceResolution = new Vector2(TARGET_WIDTH, TARGET_HEIGHT);
		canvasScaler.referencePixelsPerUnit = PIXEL_PER_UNIT;

		if ((TARGET_WIDTH / Screen.width) <= (TARGET_HEIGHT / Screen.height))
		{
			canvasScaler.matchWidthOrHeight = 1;
		}
		else
		{
			canvasScaler.matchWidthOrHeight = 0;
		}       
	}

    private void Update()
    {
        Screen.orientation = ScreenOrientation.Landscape;
    }

}
