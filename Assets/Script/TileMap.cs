using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class TileMap 
{
	public Vector2 tileSize;

	public Tile m_currentTile;
	
    public List<Tile> _tiles;//Список в котором все тайлы

    public List<Tile> _employedTiles;//Список занятых объектами тайлов

    private static TileMap instance;  	
 
	public static TileMap Instance
	{
	   get 
	   {
	       	if (instance == null)
	        {
	            instance = new TileMap();
	        }
	        return instance;
	    }
	}

	private TileMap ()
	{
		_tiles = new List<Tile>();

        _employedTiles = new List<Tile>();
    }

	public void AddTileInList(Tile tile)//Добавлять в список новый тайл
	{
		_tiles.Add(tile);
	}

    public void AddEmployedTileInList(Tile tile, int sizeX, int sizeY)//Добавлять в список новый ЗАНЯТЫЙ тайл
    {
        _employedTiles.Add(tile);

            foreach (Tile _tile in _tiles)
             {
                for (int i = 1; i < sizeX; ++i)
                {
                   if ((_tile._matrixPos.x == tile._matrixPos.x + i)&(_tile._matrixPos.y== tile._matrixPos.y))
                   {
                       _employedTiles.Add(_tile);
                   }
               }

            for (int j = 1; j < sizeY; ++j)
            {
                if ((_tile._matrixPos.y == tile._matrixPos.y + j) & (_tile._matrixPos.x == tile._matrixPos.x))
                {
                    _employedTiles.Add(_tile);
                }
            }

        }



        Debug.Log("_employedTiles.Count = " + _employedTiles.Count);

        foreach (Tile _tile in _employedTiles)
        {
            Debug.Log("_tile.matrixPosition = " + _tile._matrixPos);
        }
  }

    public void GetCurrentTile(Vector3 inputPos)//Получить тайл из списка по координатам
	{
		foreach(Tile _tile in _tiles)
		{
			if (_tile.CheckTitle (inputPos)) // здесь надо проверить принадлежность объекта к тайтлу и тогда узнаем в каком тайтле он должен быть
			{
                //Прорверка занятости этого тайла другим объектом
				m_currentTile = _tile;

				break;
			} 
		}
	}
    
	public void CreateMap(Vector3 objectPosition,Vector2 mapSize,Vector2 tileSize)
	{
		tileSize = new Vector2(tileSize.x/100f, tileSize.y/100f);//задаем размер одного тайла

        Vector3 currentPos = objectPosition;	

		for(int i = 0;i< mapSize.x;++i)
		{
			if(i == 0)
			{
				currentPos = new Vector3( currentPos.x,(currentPos.y - tileSize.y/2f) ,0);
			}
			else
			{
				currentPos = new Vector3((currentPos.x +(tileSize.x/2f )) + tileSize.x/2f * mapSize.x,(currentPos.y - tileSize.y/2f) + tileSize.y/2f * mapSize.y  ,0);
			}
			
			for(int j = 0;j<mapSize.y;++j)
			{
				 Tile tile  = new Tile(new Vector2(j,i),currentPos,tileSize);

				 AddTileInList(tile);

				currentPos  = new Vector3(currentPos.x - tileSize.x/2, currentPos.y - tileSize.y/2,0);	 
			}
 		} 
	}

	public Vector3 GetMatrixTilePosition(Vector2 matrixPos)//получить матричные координаты тайла
	{
		 foreach(Tile _tile in _tiles) 
		{	
 			if(_tile._matrixPos.Equals(matrixPos))
			{
				return new Vector3 (_tile._tilePos.x, _tile._tilePos.y, _tile._tilePos.z);
			}
		}
			return Vector3.one;
	}

    public bool CanBecomHere(Tile tile)
    {
        Debug.Log("_employedTiles.Count = " + _employedTiles.Count);

        bool result = false;

        if (_employedTiles.Count > 0)
        {
            foreach (Tile _tile in _employedTiles)
            {
                if (tile._matrixPos == _tile._matrixPos)
                {
                    result = false;
                    break;
                }
                else
                {
                    result = true;
                    break;
                }
            }
        }
        else
        {
            result = true;
        }
        
        return result;
    }
    
}
	
	 

 	
 