﻿using UnityEngine;
using System.Collections;
using System;

public static class LoadWindowManager
{
    public static void LoadAllWindowInGame()
    {
        foreach (var t in Enum.GetValues(typeof(eWindowNameInLocation)))
        {
            UnityPoolManager.Instance.PushStartScene(t.ToString(), LoadObj("Windows/" +
                    t.ToString(), GameManager.Instance.gameObject.transform).GetComponent<UnityPoolObject>());
        }

    }

    private static GameObject LoadObj(string path, Transform parent)
    {
        GameObject temp = Resources.Load(path) as GameObject;

        if (temp == null)
            Debug.LogError("load obj " + path);

		temp.SetActive(false);

        temp = GameObject.Instantiate(Resources.Load(path)) as GameObject;

        temp.transform.SetParent(parent, false);

        return temp;
    }

    public enum eWindowNameInLocation
    {

       
    }

}
