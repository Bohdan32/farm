﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{	
    private static GameManager m_instance;
    public static GameManager Instance { get { return m_instance; } set {;} }

    void Awake()
    {
        m_instance = this;
    }

    void Start ()
	{		
		LoadWindowManager.LoadAllWindowInGame ();

        
	}

    private void Update()
    {

        ScreenOrientation t = ScreenOrientation.Landscape;

        Screen.autorotateToPortrait = false;

        Screen.autorotateToPortraitUpsideDown = false;
    }

}
