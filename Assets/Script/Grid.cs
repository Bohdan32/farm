﻿using UnityEngine;
using UnityEditor;

public class Grid : MonoBehaviour
{
	public float StepX = 1f;

	public float StepY = 1f;

    public float CountLines = 10f;

    void OnDrawGizmos()
    {
		if (StepX < 0.1f) 
		{
			StepX = 0.1f;
		}

		if (StepY < 0.1f) 
		{
			StepY = 0.1f;	
		}
 
			var HalfX = (CountLines / 2f) * StepX;

            for (float x = -HalfX; x < HalfX; x += StepX)
            {
                var c = Mathf.Floor(x / StepX) * StepX + StepX / 2f;

                var a = new Vector3(c, -CountLines * StepX / 2f - StepX / 2f);

                var b = new Vector3(c, CountLines * StepX / 2f - StepX / 2f);

                Gizmos.DrawLine(a, b);
            }

			var HalfY = (CountLines / 2f) * StepY;

            for (float y = -HalfY; y < HalfY; y += StepY)
            {
                var c = Mathf.Floor(y / StepY) * StepY + StepY / 2f;

                var a = new Vector3(-CountLines * StepY / 2f - StepY / 2f, c);

                var b = new Vector3(CountLines * StepY / 2f - StepY / 2f, c);

                Gizmos.DrawLine(a, b);
            }
    }

	void Update()
	{
			foreach (var obj in Selection.objects)
			{
				Vector3 pos = (obj as GameObject).transform.position;

				float x = Mathf.CeilToInt(pos.x / StepX) * StepX;

				float y = Mathf.CeilToInt(pos.y / StepY) * StepY;

				float z = pos.z;

				(obj as GameObject).transform.position = new Vector3(x, y, z);
			}
	}
}
