using UnityEngine;

public class TileMapManager : MonoBehaviour
{
	public static TileMapManager Instance;

	[SerializeField]
	private Camera _Camera;

 	private TileMap m_tileMap;
 
	public Vector2 tileSize;

	public Vector2 mapSize;
 
	private Vector2 CursorWorldPosition
	{
		get
		{
			return _Camera.ScreenToWorldPoint(Input.mousePosition);
		}
	}

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		m_tileMap = TileMap.Instance; 
 
		m_tileMap.CreateMap(gameObject.transform.position,mapSize,tileSize);
	}
 
	void OnDrawGizmos()
	{		
		foreach(Tile tile in TileMap.Instance._tiles)
		{				
			for(int i = 0;i<tile._points.Count;++i)
			{
				if(i == tile._points.Count-1)
				{		
					Gizmos.DrawLine(tile._points[i],tile._points[0]);//первая ячейка
				}
                else
                {
					Gizmos.DrawLine(tile._points[i],tile._points[i+1]);// от точки к точке
				}
			}
		}
	}
    private void Update()
    {
        
    }
    public void BuyObjectInShop(string nameProduct)
	{
		GameObject go = Instantiate (Resources.Load(("ProductInShop/"+nameProduct),typeof(GameObject))) as GameObject;

		go.transform.parent = gameObject.transform;

		go.transform.localScale = new Vector3 (0.1f,0.1f,0.1f);

		go.SetActive (true);

		go.transform.position = CursorWorldPosition;
	}	
	
}



